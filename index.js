/**
 * Parses a document of type 'text/pdf'
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 */
const path = require('path');
const os = require('os');
const fs = require('fs');
const pdfText = require('pdf-text');
const download = require('download-file');

exports.parsePDF = (req, res) => {
    
    let data = req.rawBody;
    let tmpdir = os.tmpdir();
    let dataExportArray = [];

    let textoPDF = (data) => {
        return new Promise((result, reject) => {
            pdfText(data, (err, chunks) => {
                if (err){
                    reject(err);
                } else {
                    result(chunks);
                    console.log(chunks);
                }
                
            });
        })
    };

    let downloadTemplate = () => {
        return new Promise((result, reject) => {
            let urlTemplate = "https://storage.googleapis.com/sion-devcat-bucket/lecturas.pdf";
            let optionsTemplate = {
                directory: tmpdir,
                filename: "datos-base.pdf"
            }
            download(urlTemplate,optionsTemplate, (err) => {
                if(err){
                    reject(err);
                    console.log(err);
                    return;
                } else {
                    console.log(`exito`);
                    console.log(`Este console.log es en la funcion de descargaTemplate ${tmpdir}`);
                    //intentado imprimir con comando shell el contenido de la carpeta
                    let exec = require('child_process').exec, child;
                    child = exec('cd /tmp && ls -l', (err, stdout, stderr) => {
                        console.log(stdout);
                        if (err != null ){
                            console.log('exec error: ' + error);
                        } else {
                            result('Exito!!');
                        }
                    });
                    return;
                } 
            });
            
        });
    }

    let convertTemplate = () => {
        return new Promise ((result, reject) => {
            
            console.log(`Este console.log es en la funcion de convertirTemplate ${tmpdir}`);
            //intentado imprimir con comando shell el contenido de la carpeta
            let exec = require('child_process').exec, child;
            child = exec('cd /tmp && ls -l', (err, stdout, stderr) => {
                console.log(stdout);
                if (err != null ){
                    console.log('exec error: ' + error);
                }
            })
            
            let pathToTmeplate = "/tmp/datos-base.pdf";
            let buffer = fs.readFileSync(pathToTmeplate);
            pdfText(buffer, (err, chunks) => {
                if (err){
                    console.log(err);
                    reject(err);
                } else {
                    console.log(chunks);
                    result(chunks);
                }
            });
        });
    }


    let compareData = (resulta,resultb) => {
        return new Promise((result,reject) => {
            for(let i=0; i<resultb.length; i++){
                if (resulta.length == 0){
                    reject("Ha ocurrido un error con la plantilla");
                }
                if (resulta[i] != resultb[i]){
                    dataExportArray.push({
                        "Subido": resulta[i],
                        "Plantilla": resultb[i]
                    });
                }
                result(dataExportArray);
            }
        });
    }


    async function ejecucion(data) {
        try{
            let resulta = await textoPDF(data);
            await downloadTemplate();
            let resultb = await convertTemplate();
            let exporData = await compareData(resulta,resultb);
            res.status(200).send(exporData);
        } catch(err){
            return console.log(err.message);
        }
    }

    ejecucion(data);
  };